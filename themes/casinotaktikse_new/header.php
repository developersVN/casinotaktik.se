<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> style="background-image: url(<?php echo $header_image ?>)">
    <header class="casinotaktikse-header" id="casinotaktikse-header">
        <nav class="navbar">
          <div class="container">
            <div class="rows">
                <div class="navbar-header">
                    <button class="navbar-toggle" id="menu-button" data-toggle="collapse" data-target="#casinotaktikse-menu-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span><span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="m-logo" href="<?php echo home_url();?>">
                 </a>
                </div>
                <div class="collapse navbar-collapse" id="casinotaktikse-menu-nav">
                    <?php
                        wp_nav_menu( array(
                            'menu' => 'top_menu',
                            'theme_location' => 'primary',
                            'depth' => 2,
                            'container' => false,
                            'menu_class' => 'nav navbar-nav',
                            //Process nav menu using our custom nav walker
                            'walker' => new wp_bootstrap_navwalker())
                        );
                    ?>
                </div>
            </div>
          </div>
        </nav>
    <div class="wrap-search">
        <div class="container">
            <div class="row">
                 <div class="search-form">
                        <form role="search" method="GET"  class="searchform" action="<?php echo home_url();?>">
                            <div>
                                <label class="screen-reader-text" for="s">Search for:</label>
                                <input value="" name="s" id="s" type="text" placeholder="Enter keyword here..." aria-label="Search box">
                                <button class="search-button"  type="submit" aria-label="Search button"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</header>
  <?php 
            $header_image = get_header_image();
                if($header_image) :
        ?>
            <div class="casinotaktikse-hero-banner container">
                <div class="row">
                    <div class="desktop-logo">
                        <a href="<?php echo home_url();?>">
                             <img src="<?php echo get_theme_logo(); ?>" alt="<?php bloginfo('name'); ?>" />
                        </a>
                    </div>
                    <div style="background-image: url(<?php echo $header_image ?>)"  class="banner-image-mobile"></div>
                </div>
            </div>
        <?php endif; ?>
        
    <div class="container casinotaktikse-base-container">
            