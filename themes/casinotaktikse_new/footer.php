	</div>
	<footer class="casinotaktikse-footer" id="casinotaktikse-footer">
        <div class="container">
            <div class="casinotaktikse-copyright"><i></i>&copy; <?php echo date('Y') ?></div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>