<?php
/**
 *
 * Template Name: Home page
 * This is the template that displays home page.
 *
 */
get_header(); ?>
    <div id="casinotaktikse-primary" class="casinotaktikse-content-area">
       	<main id="casinotaktikse-main" class="casinotaktikse-site-main" >
            <?php if(has_post_thumbnail()): ?>
                <div class="page-thubnail">
                    <?php the_post_thumbnail(); ?>
                 </div>
            <?php endif; ?>
            <div class="casinotaktikse-page-title">
                <h1><?php the_title();?></h1>
            </div>
            <h2>Top List Table Example</h2>
            <?php echo do_shortcode('[casino-list]'); ?>

            <h2>Top List Boxes Example</h2>
            <?php echo do_shortcode('[casino-boxes]'); ?>
            <?php
                //Get page content
                // Start the loop.
                while ( have_posts() ) : the_post();
            ?>
                <div class="casinotaktikse-the-content">
                    <?php
                        the_content();
                    ?>
                </div>
            <?php
            // End the loop.
            endwhile;
            ?>
        </main><!-- .site-main -->
    </div><!-- .content-area -->
<?php get_footer(); ?>